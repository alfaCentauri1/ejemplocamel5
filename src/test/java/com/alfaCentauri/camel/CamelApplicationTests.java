package com.alfaCentauri.camel;

import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;

import static org.junit.Assert.assertTrue;

@ActiveProfiles("dev")
@RunWith(CamelSpringBootRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
class CamelApplicationTests {
	@Autowired
	ProducerTemplate producerTemplate;

	@Autowired
	Environment environment;

	@Test
	void testMoveFile() throws InterruptedException {
		String message = "type,sku#,itemdescription,price\n" +
				"ADD,100,Samsung TV,500\n" +
				"ADD,101,LG TV,500\n" +
				"**************************\n" +
				"Nuevo,300,Sony Smart TV,550\n" +
				"ADD,400,TCL Smart TV, 450";
		String fileName = "salidaTest.txt";
		producerTemplate.sendBodyAndHeader(environment.getProperty("fromRoute"), message, Exchange.FILE_NAME , fileName);
		Thread.sleep(3000);
		File outFile = new File("data/output/"+fileName);
		assertTrue(outFile.exists());
	}

}
